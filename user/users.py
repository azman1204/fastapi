from fastapi import APIRouter
from extension import conn

router = APIRouter()

@router.get('/users')
async def get_users():
    sql = "SELECT * FROM public.user"
    cur = conn.cursor()
    cur.execute(sql)
    users = cur.fetchall()
    return users

