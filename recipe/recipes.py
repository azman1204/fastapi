from fastapi import APIRouter
from extension import conn
from pydantic import BaseModel

router = APIRouter()

class Recipe(BaseModel):
    name: str
    description: str | None = None
    cook_time: int | None = 0

@router.get('/recipe/all')
def get_all_recipes():
    sql = "SELECT * FROM recipe"
    cur = conn.cursor()
    cur.execute(sql)
    recipes = cur.fetchall()
    return recipes, 200


@router.post('/recipes')
def post_recipe(recipe: Recipe):
    sql = "INSERT INTO recipe(name, description, cook_time) VALUES(%s, %s, %s)"
    cur = conn.cursor()
    data = (
        recipe.name, 
        recipe.description, 
        recipe.cook_time
    )
    cur.execute(sql, data)
    conn.commit()
    return recipe, 200

@router.put("/recipes/{recipe_id}")
def update_recipe(recipe_id: int, recipe: Recipe):
    sql = "UPDATE recipe SET name = %s, description = %s, cook_time = %s WHERE id = %s"
    cur = conn.cursor()
    data = (
        recipe.name, 
        recipe.description, 
        recipe.cook_time,
        recipe_id
    )
    cur.execute(sql, data)
    conn.commit()
    return recipe, 200


@router.delete("/recipes/{recipe_id}")
def delete_recipe(recipe_id: int):
    sql = f"DELETE FROM recipe WHERE id = {recipe_id}"
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    return "ok", 200