from fastapi import FastAPI
from extension import conn
from user import users
from recipe import recipes

app = FastAPI()
app.include_router(users.router)
app.include_router(recipes.router)


@app.get('/')
def hello_world():
    return {"hello", "world"}


@app.get('/items/{item_id}')
def read_item(item_id: int, name: str = None):
    return {"item_id": item_id, "name": name}
